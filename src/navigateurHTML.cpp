// Navigateur de documentation en HTML
// Invocation en ligne de commande navigateur web leger: Nyxt

// Nyxt is a Keyboard-oriented Web Browser Inspired by Emacs and Vim
// https://nyxt.atlas.engineer/
//
#include <iostream>
#include <filesystem>
#include <string>
#include <cstdlib> // Pour std::system

namespace fs = std::filesystem;

bool findHtmlFile(const fs::path& directory, std::string& foundPath) {
    for (const auto& entry : fs::recursive_directory_iterator(directory)) {
        if (entry.path().extension() == ".html") {
            foundPath = entry.path().string();
            return true;
        }
    }
    return false;
}

void openInNyxt(const std::string& filePath) {
    std::string command = "nyxt file://" + filePath;
    std::system(command.c_str());
}

int main() {
    fs::path doxygenOutputDir = "/path/to/doxygen/output";
    std::string foundHtmlPath;

    if (findHtmlFile(doxygenOutputDir, foundHtmlPath)) {
        std::cout << "Found HTML file: " << foundHtmlPath << std::endl;
        openInNyxt(foundHtmlPath);
    } else {
        std::cerr << "No HTML file found in the specified directory." << std::endl;
    }

    return 0;
}

#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <dlfcn.h> // Pour le chargement dynamique des bibliothèques partagées

// Définition de la structure d'un module
struct Module {
    void* handle; // Gestionnaire de la bibliothèque partagée
    void (*run)(); // Pointeur vers la fonction d'exécution du module
};

int main() {
    // Map pour stocker les modules chargés
    std::map<std::string, Module> modules;

    // Boucle d'interaction avec l'utilisateur
    while (true) {
        std::cout << "Entrez le nom du module à charger (ou 'exit' pour quitter) : ";
        std::string moduleName;
        std::cin >> moduleName;

        if (moduleName == "exit") {
            break;
        }

        // Tentative de chargement du module
        void* moduleHandle = dlopen(("modules/lib" + moduleName + ".so").c_str(), RTLD_LAZY);
        if (moduleHandle == nullptr) {
            std::cerr << "Impossible de charger le module." << std::endl;
            continue;
        }

        // Récupération du pointeur vers la fonction run
        void (*run)() = (void (*)())dlsym(moduleHandle, "run");
        if (run == nullptr) {
            std::cerr << "Module invalide." << std::endl;
            dlclose(moduleHandle);
            continue;
        }

        // Stockage du module dans la map
        modules[moduleName] = {moduleHandle, run};

        // Exécution du module
        run();
    }

    // Libération des modules chargés
    for (auto& pair : modules) {
        dlclose(pair.second.handle);
    }

    return 0;
}


# Support de documentation du projet QASARI
---
**NEXT-GEN CAO DESIGN & ARCHITECTURE**

## ebooks

Vous trouverez dans le dossier "ebooks" la plupart des livres au format PDF pour l'apprentissage et comme référence de documentation

## Basculer dans l'univers FOSS

* [gofoss.net](https://gofoss.net/fr/)

## Documentation des langages

* [https://devdocs.io/](https://devdocs.io/)

## Apprentissage de GNU/Linux

* [Reprenez le controle à l'aide le Linux (openclassroom)](http://sdz.tdct.org/sdz/reprenez-le-controle-a-l-aide-de-linux.html)
* [Certification LPIC-1](https://www.editions-eni.fr/livre/linux-preparation-a-la-certification-lpic-1-examens-lpi-101-et-lpi-102-6e-edition-9782409024962)
* [Administration système site de Goffinet](https://linux.goffinet.org/)

## Le Shell

* [UNIX. Pour aller plus loin avec la ligne de commande](https://archives.framabook.org/unixpou-allerplusloinaveclalignedecommande/index.html)

## Git

* [Tutorial contrôle de version avec Git & Github](https://www.pierre-giraud.com/git-github-apprendre-cours/)
* [Github handbook](https://docs.github.com/fr)
* [Pro Git](https://git-scm.com/book/fr/v2)

## Apprendre à utiliser (Neo)vim comme IDE

* [Vim pour les humains](https://vimebook.com/fr)
* [Configurer (Neo)Vim comme IDE en langage C/C++](https://legoffant.github.io/configurer-neovim-comme-ide-en-langage-cc.html)

## Choix de compilateur LLVM

Nous allons utiliser LLVM pour optimiser les performances du compilateur

* [GCC vs Clang.LLVM: An In-Depth Comparison of C/C++ Compilers](https://alibabatech.medium.com/gcc-vs-clang-llvm-an-in-depth-comparison-of-c-c-compilers-899ede2be378)
* [LLVM Clang 16 vs. GCC 13 Compiler Performance On Intel Raptor Lake](https://www.phoronix.com/review/gcc13-clang16-raptorlake)

#### Installation

```bash
sudo pacman -S llvm
```

#### Utilisation

Pour compiler on utilise l'utilitaire en front-end `clang`

hello.cpp
```cpp
#include <iostream>

int main() {

   std::cout << "Hello World!";
   return 0;
}

```

```bash
clang++ hello.cpp -o hello -Wall -Wextra -std=c++23
```


## Langage C

* [Apprendre le C en 20 heures](https://archives.framabook.org/docs/c20h/C20H_integrale_creative-commons-by-sa.pdf)

## Makefile

* [Introduction à Makefile](https://gl.developpez.com/tutoriel/outil/makefile/)

## Basics des Algorithmes

* [Algorithmique pour l'apprenti programmeur](https://zestedesavoir.com/tutoriels/621/algorithmique-pour-lapprenti-programmeur/)

## Langage C++

* [tutorial learncpp](https://www.learncpp.com/)
* [Code Beauty](https://www.youtube.com/c/CodeBeauty)
* [Ressources Hacking CPP](https://hackingcpp.com/)
* [Initiation à la programmation en C++ - Coursera EPFL](https://www.coursera.org/learn/initiation-programmation-cpp)
* [La programmation en C++ moderne](https://zestedesavoir.com/tutoriels/822/la-programmation-en-c-moderne/)
* [Learn C++ Programming for beginners - Free 31 Hour Course](https://www.freecodecamp.org/news/learn-c-with-free-31-hour-course/)
* [Livre C++ Primer](https://www.amazon.fr/C-Primer-Stanley-B-Lippman/dp/0321714113)

## Apprentissage orienté projet

Faire soi-même plusieurs petits projets dont:

* Space Invaders avec SFML
* Game Engine (Pikuma tuto)

Gérer le projet avec Cmake:

* [Préparation de projet avancée en C++](https://legoffant.github.io/preparation-de-projet-avancee-en-c.html)

## Eco-système et environnement C++

* [C++ Ecosystème](https://legoffant.github.io/c-eco-systeme.html)

On peut trouver des statistiques sur JetBrain: https://www.jetbrains.com/lp/devecosystem-2019/cpp/

Ainsi on découvre la part de marché chez les dev des compileurs:

* GCC 66%
* Clang 32%
* MSVC(Microsoft) 30%

Egalement quels sont les éditeurs de texte utilisés:

* Visual Studio 27%
* CLion 22%
* VSCode 18%
* Vi/Vim 7%

Les systèmes de Builds:

* CMake 42%
* Visual studio project 37%
* Makefile 33%

On peut coupler ces statistiques à l'étude des pratiques des développeurs de StackOverFlow et obtenir en complément: https://insights.stackoverflow.com/survey/

**CONCLUSION ENVIRONNEMENT**: `Linux + GCC + Neovim + Cmake + PostgreSQL ou MongoDB`

## Algorithmes et structures de données

* [Data Structures and Algorithm in C++ (2nd Edition); Goodrich, Tamassia, Mount](https://www.amazon.com/Data-Structures-Algorithms-Michael-Goodrich/dp/0470383275)
* [C++ Data Structures and Algorithms Cheat Sheet](https://github.com/gibsjose/cpp-cheat-sheet/blob/master/Data%20Structures%20and%20Algorithms.md)
* [Learn DS & Algorithms](https://www.programiz.com/dsa)

## Design Patterns

* [Les patrons de conception en C++](https://refactoring.guru/fr/design-patterns/cpp)
* [Design Patterns in Modern C++ (PDF)](https://www.programmer-books.com/wp-content/uploads/2019/03/Design-Patterns-in-Modern-C.pdf)

## Intégration de l'intelligence artificielle

Pour le projet, on commence à coder à la sortie de GPT-5 et son intégration avec Vim tel que l'outil "vim-ai" dans les plugins

## Choix d'interface GUI

Pour l'écriture du logiciel QASARI nous allons utiliser [Dear ImGui](https://github.com/ocornut/imgui) - Donc autant que vous soyez habituer à son utilisation

* [Dear ImGui a interactive manual](https://pthom.github.io/imgui_manual_online/manual/imgui_manual.html)
* [imgui documenation (PDF)](https://readthedocs.org/projects/imgui-test/downloads/pdf/latest/)

### OpenGL

OpenGL (Open Graphics Library) est une spécification qui définit une API multiplate-forme pour la conception d'applications générant des images 3D (mais également 2D). Elle utilise en interne les représentations de la géométrie projective pour éviter toute situation faisant intervenir des infinis.

L'interface regroupe environ 250 fonctions différentes qui peuvent être utilisées pour afficher des scènes tridimensionnelles complexes à partir de simples primitives géométriques. Du fait de son ouverture, de sa souplesse d'utilisation et de sa disponibilité sur toutes les plates-formes, elle est utilisée par la majorité des applications scientifiques, industrielles ou artistiques 3D et certaines applications 2D vectorielles

### Vulkan

Également connue sous le nom de VulkanRT, Vulkan Run Time Libraries est une API graphique 3D fournie par Khronos Group Inc.

Pour aller plus loin, vous pouvez comparer ces bibliothèques à OpenGL ou DirectX, qui aident les PC à afficher des données graphiques en haute définition, en couleur et en clarté.

Elle fournit une bien meilleure abstraction des cartes graphiques modernes. Cette nouvelle interface vous permet de mieux décrire ce que votre application souhaite faire, ce qui peut mener à de meilleures performances et à des comportements moins variables comparés à des APIs existantes comme OpenGL et Direct3D

* [Vulkan Tutorial](https://vulkan-tutorial.com/)
* [Implementing Dear ImGui to Vulkan Engine](https://vkguide.dev/docs/extra-chapter/implementing_imgui/)
* [Vulkan Guide](https://github.com/mikeroyal/Vulkan-Guide)

**NOTA**: En C++ pour améliorer les performances de gestion mémoire utiliser la technique du RAII (Ressource Acquisition Is Initialization) et des smarts pointers.

## Jalon des exigences du projet QASARI

- [ ] Conception de la brique principale logiciel "Core", assistance CLI Avec un I.A
- [ ] Git refonte intégration avec gestion de base de donnée pour des fichiers d'objets et élements
- [ ] Support de la conception architecturale openBIM et évolution du format STEP
- [ ] Gestionnaire de module et script pour en créer
- [ ] Geometric Modeling kernel et rendu 3D basé sur des moteurs de jeux-vidéos
- [ ] Blockchain pour la paternité d'une invention dans des registres
- [ ] Gestion et automatisation de l'invention avec le Machine Learning et TRIZ
- [ ] Module de prototypage rapide et G-Code
- [ ] Calcul distribué et parallèle pour HPC pour FEA, CFD type projet BOINC
- [ ] Bibliothèque de composant et élement standardisé mondial et partagé
- [ ] Algorithmes et génération procédurale en natif pour la gestion d'automates cellulaires et la conception de nanites

## Geometric Modeling Kernel

C'est un élement essentiel des logiciels de CAO à travers des mathématiques pour avoir un rendu 3D solide. On peut noter plusieurs type tel que:

* Open Cascade Modeling kernel
* CGAL Computational Geometry Library
* Boost.Geometry 

Pour le projet on ce réfère à l'intégration de [LibFive](https://libfive.com/).

> libfive is a software library and set of tools for solid modeling, especially suited for parametric and procedural design. It is infrastructure for generative design, mass customization, and domain-specific CAD tools.

```
git clone https://github.com/libfive/libfive
```

**Livres ressources**

* Geometric Tools for Computer Graphics; Schneider, Eberly
* Geometric Modeling; Mortenson 2d
* Pratical Geometry Algorithms with C++ Code; Daniel Sunday

## Liste des modules "CORE"

1. **Initialisation du Projet** :
    - Module d'initialisation du projet : Créez un nouveau projet de CAO, configurez les paramètres initiaux, gérez les fichiers de projet et configurez l'environnement de travail.

2. **Gestion des Nanites** :
    - Module de création de nanites : Permet la création, l'édition et l'organisation des nanites, qui servent d'éléments de base pour la modélisation.
    - Module de transformation : Gère les transformations géométriques (translation, rotation, mise à l'échelle) pour les nanites et les objets.
    - Module d'interaction avec les nanites : Pour interagir avec les nanites via une interface utilisateur, y compris la sélection, la modification et la suppression.
    - Module de cotation automatique : Pour générer automatiquement des cotations sur les dessins à l'aide des nanites.
    - Module de loi d'évolution de TRIZ pour gérer l'automatisation des nanites

3. **Scripts Personnalisés en Lua** :
    - Module d'exécution de scripts Lua : Charge, exécute et interagit avec des scripts Lua personnalisés. Fournit une interface pour les scripts Lua pour interagir avec l'ensemble de l'application, y compris le moteur de modélisation et le générateur procédural. Implémentation: LuaBridge comme "Binding C++"
    - Permet d'écrire également en mode console CLI des formes complexes et personnalisés

4. **Interface Utilisateur** :
    - Module d'interface utilisateur : Crée et gère l'interface utilisateur de l'application, y compris les panneaux de dessin, les menus, les boîtes de dialogue et les outils de dessin. Implementation: Dear ImGui
    - Intégration de Lua dans l'interface utilisateur : Permet aux utilisateurs d'utiliser des scripts Lua directement à partir de l'interface utilisateur. 

5. **Gestion des Fichiers** :
    - Module de gestion de fichiers : Gère le chargement, l'enregistrement et la gestion des fichiers de dessins et de projets.
    - Gestion de conversion des .STEP amelioré

6. **Optimisation et Performance** :
    - Module d'optimisation de performance : Veille à ce que l'application fonctionne de manière fluide, même avec des dessins complexes.

7. **Moteur de Modélisation 2D/3D** :
    - Moteur de modélisation : Inclut les composants pour la création, la modification et la visualisation de modèles en 2D et en 3D. Gère les objets, les matériaux, les éclairages, les textures, etc. Implementation: Vulkan + GLFW + GLM (vkvg pour le 2D vectoriel)
    - Intégration de nanites : Permet l'utilisation des nanites comme composants du moteur de modélisation, permettant la création de modèles complexes.
    - Construit comme un moteur de jeu.

8. **Génération Procédurale** :
    - Module de génération procédurale : Permet la création de modèles et de formes géométriques à l'aide d'algorithmes de génération procédurale. Il peut utiliser des scripts personnalisés en Lua pour définir ces algorithmes. Implementation: Boost.Geometry et openFrameworks (OF) (equivalent à processing).

9. **Kernel Géométrique** :
    - Kernel géométrique : Fournit des fonctionnalités essentielles pour le traitement et la manipulation des données géométriques, y compris des opérations de géométrie de base, de modélisation, et de rendu.
    - Native utilise la generation procédurale basée sur les automates cellulaires. Implémentation: Libfive

10. **Base de Données** :

    - Module de gestion de base de données : Intègre un système de gestion de base de données pour stocker et gérer des projets, des modèles, des scripts, des métadonnées, et d'autres informations pertinentes. Assurez-vous que la base de données est capable de gérer les opérations CRUD (Create, Read, Update, Delete) et de fournir des fonctionnalités de recherche et de requête. Implémentation: SurrealDB pour de la graph database noSQL.

11. **Documentation et Aide** :
     - Module de documentation : Fournit de l'aide et de la documentation aux utilisateurs, y compris des tutoriels pour l'utilisation des nanites, des scripts Lua, de la génération procédurale, et du kernel géométrique. Implémentation: Doxygen + Wiki + CLI

12. **Tests et Débogage** :
     - Module de tests et de débogage : Facilite les tests unitaires, le débogage, et l'assurance qualité de l'ensemble de l'application.

12. **Sauvegarde et Contrôle de Version** :
     - Module de sauvegarde et de contrôle de version : Gère les versions et la sauvegarde des dessins, des projets, et des données générées par la génération procédurale. Implémentation: Perforce Helix Core pour gérer des .STEP amelioré en binaire.

## Ressource d'apprentissage moteur 2D/3D et génération procédurale

### Apprendre à créer un moteur 2D

Créer un moteur 2D pour les jeux vidéo est un excellent moyen d'apprendre les fondamentaux de la programmation de jeux, de la gestion des graphismes et de l'optimisation. Voici une liste de ressources pour vous aider à apprendre à créer un moteur 2D pour les jeux :

**Tutoriels en ligne** :

1. **LazyFoo's SDL Tutorials** : Une série de tutoriels qui couvrent la création d'un moteur de jeu 2D à l'aide de la bibliothèque SDL (Simple DirectMedia Layer). Site Web : [LazyFoo.net](http://lazyfoo.net/SDL_tutorials/)

2. **SFML Tutorials** : Des tutoriels pour créer des jeux 2D avec la bibliothèque SFML (Simple and Fast Multimedia Library). Site Web : [SFML Tutorials](https://www.sfml-dev.org/tutorials/2.5/)

3. **Phaser.io Tutorials** : Des tutoriels pour créer des jeux 2D basés sur le framework Phaser, qui utilise JavaScript. Site Web : [Phaser.io Tutorials](https://phaser.io/learn)

4. **GameFromScratch Tutorials** : Une variété de tutoriels sur le développement de jeux, y compris des moteurs 2D. Site Web : [GameFromScratch](http://www.gamefromscratch.com/)

**Livres** :

1. **"SFML Game Development" par Jan Haller, Henrik Vogelius Hansson, et Artur Moreira** : Ce livre vous guide dans le développement de jeux 2D en utilisant la bibliothèque SFML.

2. **"Beginning C++ Through Game Programming" par Michael Dawson** : Ce livre vous apprendra à programmer en C++ en développant des jeux 2D simples.

3. **"2D Game Development with LibGDX" par Lee Stemkoski** : Si vous préférez utiliser LibGDX, ce livre vous montrera comment créer des jeux 2D multiplateformes.

**Cours en ligne** :

1. **Coursera - "Game Development for Modern Platforms"** : Ce cours vous apprendra à développer des jeux 2D pour différentes plates-formes en utilisant Unity.

2. **Udemy - "Complete C# Unity Game Developer 2D"** : Un cours qui couvre le développement de jeux 2D avec Unity et C#.

3. **Pikuma - "C++ Game Engine Programming"** : Apprendre les fondamentaux du 2D game engine en utilisant C++, SDL, et Lua scripting

**Forums et Communautés** :

1. **Stack Overflow - Game Development Section** : Un endroit utile pour poser des questions et obtenir de l'aide concernant le développement de jeux.

2. **gamedev.net** : Une communauté de développeurs de jeux où vous pouvez trouver des tutoriels, des articles et des forums de discussion.

**Documentation des bibliothèques** :

1. **Documentation de SDL** : La documentation officielle de la bibliothèque SDL est un excellent point de départ pour apprendre à utiliser SDL dans le développement de jeux 2D.

2. **Documentation de SFML** : Si vous utilisez SFML, la documentation officielle est une ressource précieuse pour apprendre à créer des jeux 2D.

N'oubliez pas que la création d'un moteur de jeu 2D peut être un processus complexe, mais c'est une expérience d'apprentissage précieuse. Commencez par des projets simples, suivez les tutoriels et les ressources en ligne, et progressez à mesure que vous acquérez de l'expérience. La pratique constante est la clé pour devenir un développeur de jeux compétent.

### Gérer un moteur 3D Avec Vulkan

L'utilisation de Vulkan avec GLFW et GLM pour le développement de moteurs graphiques est une approche avancée pour créer des applications graphiques hautes performances. Voici quelques ressources pour vous aider à apprendre à utiliser ces technologies ensemble :

**Tutoriels en ligne** :

1. **Vulkan Tutorial** : Un tutoriel en ligne complet qui couvre les bases de Vulkan et propose des exemples de code pour vous aider à démarrer. Site Web : [Vulkan Tutorial](https://vulkan-tutorial.com/)

2. **Learn OpenGL** : Bien que le site soit axé sur OpenGL, il dispose de sections qui couvrent l'utilisation de GLFW et GLM, qui sont également utiles lors de l'utilisation de Vulkan. Site Web : [Learn OpenGL](https://learnopengl.com/)

3. **Vulkan API Overview** : La documentation officielle de Vulkan fournit une vue d'ensemble complète de l'API, des guides et des exemples. Site Web : [Vulkan Documentation](https://www.khronos.org/registry/vulkan/)

4. **GLFW Documentation** : La documentation officielle de GLFW offre des informations détaillées sur la bibliothèque et son utilisation. Site Web : [GLFW Documentation](https://www.glfw.org/documentation.html)

5. **GLM Documentation** : La documentation officielle de GLM fournit des informations sur la bibliothèque de mathématiques GLM. Site Web : [GLM Documentation](https://glm.g-truc.net/0.9.9/index.html)

**Livres** :

1. **"Vulkan Programming Guide" par Graham Sellers, John M. Kessenich, et Bill Licea-Kane** : Ce livre couvre en profondeur le développement avec Vulkan, y compris l'utilisation de GLFW et GLM.

**Cours en ligne** :

1. **Udemy - "Vulkan API: The Complete Guide"** : Ce cours complet vous apprendra tout sur l'utilisation de Vulkan, y compris son intégration avec GLFW et GLM.

2. **Udemy - "Computer Graphics with Modern OpenGL and C++"** : Ce cours couvre OpenGL, mais il peut être utile pour comprendre les concepts de base avant de passer à Vulkan.

**Forums et Communautés** :

1. **Vulkan Forum** : Une communauté de développeurs Vulkan où vous pouvez poser des questions, partager des connaissances et discuter des défis de développement. Site Web : [Vulkan Forum](https://www.khronos.org/message_boards/)

2. **GLFW Community** : La communauté autour de GLFW est un endroit utile pour obtenir de l'aide et discuter de l'utilisation de la bibliothèque. Site Web : [GLFW Community](https://www.glfw.org/community.html)

N'oubliez pas que l'utilisation de Vulkan peut être complexe, en particulier si vous débutez dans le développement graphique. Il est recommandé de commencer par des tutoriels et des exemples simples, puis de progresser vers des projets plus complexes à mesure que vous acquérez de l'expérience. La pratique constante est essentielle pour maîtriser Vulkan, GLFW et GLM.

### Explorer la génération procédurale

L'apprentissage des algorithmes de génération procédurale peut être passionnant et utile pour divers domaines, de la génération de paysages dans les jeux vidéo à la création de modèles pour la conception assistée par ordinateur. Voici quelques ressources pour vous aider à apprendre les algorithmes de génération procédurale :

**Livres** :

1. **"Procedural Generation in Game Design" par Tanya X. Short et Tarn Adams** : Ce livre se concentre sur l'application de la génération procédurale dans la conception de jeux, couvrant une variété d'algorithmes et d'exemples concrets.

2. **"Texturing and Modeling: A Procedural Approach" par David S. Ebert et al.** : Un livre qui aborde les aspects mathématiques et algorithmiques de la génération procédurale pour la modélisation et la texturation.

**Tutoriels en ligne et cours** :

1. **Red Blob Games** : Un site Web proposant une variété de tutoriels sur les algorithmes de génération procédurale, y compris la génération de cartes, la génération de labyrinthes, et plus encore. Site Web : [Red Blob Games](https://www.redblobgames.com/)

2. **The Nature of Code** : Une série de tutoriels et de livres en ligne par Daniel Shiffman qui explore la génération procédurale et d'autres concepts mathématiques dans le contexte de la programmation créative. Site Web : [The Nature of Code](https://natureofcode.com/)

3. **Catlike Coding** : Des tutoriels sur la génération procédurale en utilisant Unity, notamment la création de terrains, de bruit de Perlin, de cartes et plus encore. Site Web : [Catlike Coding](https://catlikecoding.com/unity/tutorials/)

4. **Fast Noise Generation** : Un cours en ligne gratuit sur la génération de bruit et de textures procédurales, qui explique les mathématiques derrière ces algorithmes. Site Web : [Fast Noise Generation](http://freespace.virgin.net/hugo.elias/models/m_perlin.htm)

**Forums et communautés** :

1. **r/proceduralgeneration (Reddit)** : Une communauté de passionnés de génération procédurale où vous pouvez poser des questions, partager vos projets et discuter des dernières avancées dans le domaine. Site Web : [r/proceduralgeneration](https://www.reddit.com/r/proceduralgeneration/)

2. **Stack Overflow - Procedural Generation Section** : Un endroit pour poser des questions techniques spécifiques à la programmation de génération procédurale. Site Web : [Procedural Generation Section](https://stackoverflow.com/questions/tagged/procedural-generation)

**Outils et bibliothèques** :

1. **Libraries and Tools for Procedural Generation** : Une liste de bibliothèques, outils et ressources pour la génération procédurale sur GitHub. Site Web : [Procedural Generation GitHub List](https://github.com/ellisonleao/magictools)

2. **Ken Perlin's Noise Page** : Le site Web de Ken Perlin, l'inventeur du bruit de Perlin, propose des informations et des exemples sur la génération de bruit. Site Web : [Ken Perlin's Noise Page](https://mrl.nyu.edu/~perlin/noise/)

La génération procédurale est un domaine vaste, et il existe de nombreuses techniques et algorithmes à explorer. Commencez par des concepts de base, puis élargissez vos connaissances au fur et à mesure que vous gagnez en expérience. Expérimentez et créez vos propres projets pour mettre en pratique ce que vous apprenez.

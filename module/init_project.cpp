#include <vector>
#include <iostream>

struct Project {
    std::string projectName;
    std::vector<Nanite> nanites; // Liste des nanites du projet
    // ... autres propriétés ...
};


std::vector<Project> projects; // Un conteneur pour stocker les projets

// Fonction pour créer un nouveau projet
void createProject(const std::string& projectName) {
    Project newProject;
    newProject.projectName = projectName;
    // ... initialisation d'autres propriétés ...
    projects.push_back(newProject);
    std::cout << "Projet '" << projectName << "' créé avec succès." << std::endl;
}

// Fonction pour ajouter des nanites à un projet existant
void addNaniteToProject(const std::string& projectName, const Nanite& nanite) {
    for (Project& project : projects) {
        if (project.projectName == projectName) {
            project.nanites.push_back(nanite);
            std::cout << "Nanite ajoutée au projet '" << projectName << "'." << std::endl;
            return;
        }
    }
    std::cerr << "Projet '" << projectName << "' non trouvé." << std::endl;
}


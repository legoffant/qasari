extern "C" {
    #include "lua.h"
    #include "lualib.h"
    #include "lauxlib.h"
}

lua_State* L = luaL_newstate();
luaL_openlibs(L);

#include "LuaBridge/LuaBridge.h"

// Exemple : Exposer une fonction C++ pour créer un nouveau projet depuis un script Lua
luabridge::getGlobalNamespace(L)
    .beginNamespace("Framework")
        .addFunction("createProject", &createProject)
    .endNamespace();

// Charger et exécuter un script Lua depuis un fichier
if (luaL_dofile(L, "script.lua") != 0) {
    std::cerr << "Erreur lors de l'exécution du script Lua : " << lua_tostring(L, -1) << std::endl;
    lua_pop(L, 1); // Supprimer le message d'erreur de la pile Lua
}

// Appel d'une fonction Lua depuis C++
luabridge::LuaRef result = luabridge::getGlobal(L, "myLuaFunction")(arg1, arg2);


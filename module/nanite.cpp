#include <vector>

struct Nanite {
    // Propriétés de la nanite
    double x, y, z; // Coordonnées
    double width, height, depth; // Dimensions
    // ... d'autres propriétés ...
};


std::vector<Nanite> nanites; // Un conteneur pour stocker les nanites

// Fonction pour créer une nouvelle nanite
void createNanite(double x, double y, double z, double width, double height, double depth) {
    Nanite newNanite;
    newNanite.x = x;
    newNanite.y = y;
    newNanite.z = z;
    newNanite.width = width;
    newNanite.height = height;
    newNanite.depth = depth;
    // ... initialisation d'autres propriétés ...
    nanites.push_back(newNanite);
}

// Fonction pour lister toutes les nanites
void listNanites() {
    for (const Nanite& nanite : nanites) {
        // Affichez les propriétés de chaque nanite
        // Vous pouvez personnaliser le format de sortie ici
    }
}

// Autres fonctions pour modifier, supprimer et gérer les nanites


# qasari

Next-Gen CAO

`Qasari CLI framework for CAD` est un projet logiciel en C/C++ d'architecture & modélisation orienté-objet collaborative. Le concept est de pouvoir participer à des projets de conception modulaire à grande échelle sur des systèmes complexes.

## Genèse

L'intérêt principal est de pouvoir faire un contrôle de version dans une base de donnée de fichier IFC (Industry Foundation Classes) qui doivent être étendu non seulement dans les batiments mais à toutes architectures de système tel que naval ou mécanique allant de l'automobile à l'aérospatiale. Les fonctions principales du framework doient inclures:

**Fonction "Core"**:

* Git pour de la base de donnée BIMserver.
* Gestionnaire de module
* Console CLI
* Explorateur de fichiers
* Conversion de formats
* Geométrie Kernel
* ChatBot IA
* API

**Modules:**

* CAD Engine, modélisation du système solaire et des flux de machines. Interface GUI via Dear ImGui sous Vulkan.
* Calcul distribué et parallèle pour les opérations lourdes (FEA, CFD) tel que le projet BOINC
* Monétisation des inventions avec des Smart Contracts pour garantir la paternité.
* Automatisation de l'innovation et la méthode TRIZ via du ML sur les bases de données de brevet
* Gestion du prototypage rapide et G-Code
* Prend en charge en natif la génération procédurale pour les automates cellulaires et la conception de nanites

Etc... Quelques exemples de modules spécifique à intégrer dans la version de base.

## Business Model

Qasari est un logiciel libre sous licence GPL. Les fonctions de base sont la collaboration sur des systèmes complexes en mettant à jours des fichiers IFC à partir du modeleur. Certain modules et fonctions avancée nécessite une entreprise et payer des services spécifique. De plus un service de formation sur le logiciel, sa maintenance permet de payer des consultants et développeurs. Le modèle économique inspiré par Rapid7 / Metasploit.

## Roadmap

Je mets à dispostion de la documentation pour les personnes intéressés sur le projet sur ce repo, tel que l'apprentissage de l'informatique, la programmation en C++ mais également le Game Dev et les connaissances sur les Geometric Modeling Kernel.

## Note de compilation

```bash
clang++ -o framework src/main.cpp -ldl -rdynamic
clang++ -shared -o modules/libtest.so modules/test.cpp

Exécutez le framework en tapant ./framework. Vous pouvez charger des modules en entrant leur nom, par exemple, "test".

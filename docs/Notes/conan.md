Lors de l'utilisation de Conan pour gérer les dépendances, les bibliothèques comme LuaBridge ne sont pas installées directement dans votre projet, mais plutôt dans un cache Conan global sur votre système. Conan gère ce cache et crée des fichiers de configuration et des dossiers spécifiques pour votre projet pour inclure et lier correctement les bibliothèques.

### Processus d'installation de LuaBridge avec Conan

1. **Conan Cache** :
   - Les bibliothèques téléchargées par Conan sont stockées dans un cache global sur votre système, typiquement situé dans `~/.conan` (sous Linux/macOS) ou `C:\Users\<username>\.conan` (sous Windows).

2. **Dossier du projet** :
   - Dans votre projet, Conan crée un répertoire `build` (ou tout autre nom que vous choisissez) où il place les fichiers de configuration nécessaires pour intégrer les bibliothèques dans votre projet.
   - Lors de l'exécution de la commande `conan install`, Conan génère des fichiers de configuration (comme `conanbuildinfo.cmake`) dans ce répertoire `build`.

### Étape par étape

Voici un résumé détaillé du processus avec les étapes et la structure de votre projet :

1. **Arborescence de votre projet avant l'installation de Conan** :
   ```plaintext
   my_project/
   ├── src/
   │   ├── main.cpp
   │   └── lua.cpp
   ├── CMakeLists.txt
   └── conanfile.txt
   ```

2. **Contenu de `conanfile.txt`** :
   ```plaintext
   [requires]
   luabridge/2.7.0

   [generators]
   cmake
   ```

3. **Commande pour installer les dépendances** :
   ```bash
   conan install . --build=missing
   ```
   - Cela va télécharger LuaBridge et ses dépendances dans le cache global Conan et générer les fichiers nécessaires dans le répertoire `build`.

4. **Arborescence de votre projet après l'installation de Conan** :
   ```plaintext
   my_project/
   ├── src/
   │   ├── main.cpp
   │   └── lua.cpp
   ├── build/
   │   ├── conanbuildinfo.cmake
   │   ├── conaninfo.txt
   │   └── graph_info.json
   ├── CMakeLists.txt
   └── conanfile.txt
   ```

5. **Contenu de `CMakeLists.txt`** :
   ```cmake
   cmake_minimum_required(VERSION 3.15)
   project(MyProject)

   include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
   conan_basic_setup()

   add_executable(my_project src/main.cpp src/lua.cpp)

   target_link_libraries(my_project ${CONAN_LIBS})
   ```

6. **Construire votre projet** :
   ```bash
   mkdir build
   cd build
   cmake ..
   cmake --build .
   ```

### Résumé

- **Conan Cache** : Les bibliothèques comme LuaBridge sont téléchargées et stockées dans le cache Conan global sur votre système.
- **Fichiers de configuration** : Conan génère des fichiers de configuration (comme `conanbuildinfo.cmake`) dans le répertoire `build` de votre projet.
- **Intégration avec CMake** : Vous incluez et utilisez ces fichiers de configuration dans votre `CMakeLists.txt` pour lier les bibliothèques nécessaires.

Ainsi, les bibliothèques gérées par Conan ne sont pas "installées" directement dans votre projet, mais plutôt gérées par Conan de manière à ce qu'elles soient facilement accessibles et utilisables lors de la compilation de votre projet.
Oui, il existe plusieurs bibliothèques en C++ qui intègrent des fonctionnalités de génération procédurale pour la création de contenu 2D et 3D. Ces bibliothèques sont utilisées dans divers domaines, notamment les jeux vidéo, la modélisation, la simulation et la visualisation. Voici quelques-unes de ces bibliothèques populaires :

* OpenFrameworks :

OpenFrameworks est une bibliothèque créative C++ open source qui offre une variété d'outils pour la génération procédurale, la création graphique interactive et la programmation créative en général.

* Cinder :

Cinder est une autre bibliothèque C++ open source axée sur la création graphique et la génération procédurale. Elle est souvent utilisée dans le domaine des arts interactifs et de la visualisation.

* Procedural Generation in Game Development (libtcod) :

Libtcod est une bibliothèque C++ spécialisée dans la génération procédurale de contenu pour les jeux vidéo, notamment la génération de cartes, de paysages et de donjons.

* FastNoise :

FastNoise est une bibliothèque légère en C++ pour la génération de bruit procédural, un composant courant dans de nombreux algorithmes de génération procédurale.

* OpenSimplex Noise :

OpenSimplex Noise est une implémentation de l'algorithme de bruit OpenSimplex en C++, souvent utilisé pour générer des terrains, des textures et d'autres formes procédurales.

* Voronoi Diagram Generators :

Il existe plusieurs bibliothèques C++ pour générer des diagrammes de Voronoi, ce qui est utile dans divers contextes, y compris la génération procédurale.

* Boost Geometry :

Boost Geometry est une bibliothèque C++ qui offre des fonctionnalités pour la manipulation de géométrie, y compris des algorithmes de génération de formes géométriques.

Ces bibliothèques sont conçues pour faciliter la génération de contenu procédural, mais la sélection de la bibliothèque dépendra de vos besoins spécifiques et de la nature de votre projet. Vous pouvez les explorer pour déterminer celle qui convient le mieux à votre application.
